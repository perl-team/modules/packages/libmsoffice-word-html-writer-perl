libmsoffice-word-html-writer-perl (1.10-1) unstable; urgency=medium

  * Import upstream version 1.10.
  * Update (test) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 Sep 2023 00:24:14 +0200

libmsoffice-word-html-writer-perl (1.08-1) unstable; urgency=medium

  * Import upstream version 1.08.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Wed, 22 Mar 2023 22:18:21 +0100

libmsoffice-word-html-writer-perl (1.07-1) unstable; urgency=medium

  * Import upstream version 1.07.
  * Add debian/upstream/metadata.
  * Update years of upstream copyright.
  * Update (build) dependencies: drop libencode-perl.

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 Feb 2022 17:23:54 +0100

libmsoffice-word-html-writer-perl (1.06-1) unstable; urgency=medium

  [ Jenkins ]
  * Update standards version to 4.6.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 1.06.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 03 Feb 2022 20:24:14 +0100

libmsoffice-word-html-writer-perl (1.05-1) unstable; urgency=medium

  * Import upstream version 1.05.
  * Update years of packaging copyright, and upstream email address.
  * Explicitly (build) depend on Scalar::Util.
  * Declare compliance with Debian Policy 4.5.1.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Thu, 25 Feb 2021 19:55:19 +0100

libmsoffice-word-html-writer-perl (1.04-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian spelling-error-in-description warning

  [ gregor herrmann ]
  * Import upstream version 1.04.
  * Update (build) dependencies.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 01:14:32 +0100

libmsoffice-word-html-writer-perl (1.03-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 May 2015 22:01:34 +0200

libmsoffice-word-html-writer-perl (1.03-1) unstable; urgency=medium

  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Update years of packaging copyright.
  * Build-depend on newer Module::Build.

 -- gregor herrmann <gregoa@debian.org>  Sat, 10 May 2014 13:33:11 +0200

libmsoffice-word-html-writer-perl (1.02-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * Update build dependencies.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Wed, 25 Dec 2013 19:39:38 +0100

libmsoffice-word-html-writer-perl (1.01-1) unstable; urgency=low

  * New upstream release.
  * Move Module::Build from B-D-I to Build-Depends (needed during clean)
    and add newer perl as an alternative.

 -- gregor herrmann <gregoa@debian.org>  Fri, 30 Sep 2011 17:04:28 +0200

libmsoffice-word-html-writer-perl (1.00-1) unstable; urgency=low

  * Initial release (closes: #638592).

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Aug 2011 03:08:12 +0200
